#include "upl_math.h"
#include <math.h>

float sinus(float x = 3.141592) {
    return (float)::sin(x);
}

float cosinus(float x = 3.141592) {
    return (float)::cos(x);
}

float tangent(float x = 3.141592) {
    return (float)::tan(x);
}

float square_root(float x = 0.0) {
    return (float)::sqrt(x);
}

float arcsinus(float x) {
    return (float)::asin(x);
}

float arccosinus(float x) {
    return (float)::acos(x);
}

float arctangent(float x) {
    return (float)::atan(x);
}

float arctangent2(float a, float b) {
    return (float)::atan2(a, b);
}

float absolute_value(float x) {
    if (x < 0)
        return -x;
    return x;
}

float ceiling(float x) {
    return (float)::ceil(x);
}

float flooring(float x) {
    return (float)::floor(x);
}

float minimum(float a, float b) {
    if (a < b)
        return a;
    return b;
}

float maximum(float a, float b) {
    if (a > b)
        return a;
    return b;
}
