#include <map>
#include <vector>
#include <string>
#include <cstring>
#include <bits/stdc++.h>
#include "main.h"
#include "upl_math.h"
#include "UHCPlugin.h"

using namespace syscalls;

typedef int player_id;
typedef syscalls::string pstring;
typedef std::string cstring;

std::map<player_id, std::vector<int>> mVillagerList;
std::vector<int> vVillagerList1, vVillagerList2, vVillagerList3, vVillagerList4, vVillagerList5, vVillagerList6, vVillagerList7,
                 vVillagerList8, vVillagerList9, vVillagerList10, vVillagerList11, vVillagerList12, vVillagerList13, vVillagerList14;

std::map<player_id, std::vector<int>> mResourceList;
std::vector<int> vResourceList1, vResourceList2, vResourceList3, vResourceList4, vResourceList5, vResourceList6, vResourceList7,
                 vResourceList8, vResourceList9, vResourceList10, vResourceList11, vResourceList12, vResourceList13, vResourceList14;

std::map<player_id, std::vector<int>> mEasyList;
std::vector<int> vEasyList1, vEasyList2, vEasyList3, vEasyList4, vEasyList5, vEasyList6, vEasyList7,
                 vEasyList8, vEasyList9, vEasyList10, vEasyList11, vEasyList12, vEasyList13, vEasyList14;

std::map<player_id, std::vector<int>> mTreeList;
std::vector<int> vTreeList1, vTreeList2, vTreeList3, vTreeList4, vTreeList5, vTreeList6, vTreeList7,
                 vTreeList8, vTreeList9, vTreeList10, vTreeList11, vTreeList12, vTreeList13, vTreeList14;

std::map<player_id, std::vector<int>> mHuntList;
std::vector<int> vHuntList1, vHuntList2, vHuntList3, vHuntList4, vHuntList5, vHuntList6, vHuntList7,
                 vHuntList8, vHuntList9, vHuntList10, vHuntList11, vHuntList12, vHuntList13, vHuntList14;

std::map<player_id, std::vector<int>> mTreasureList;
std::vector<int> vTreasureList1, vTreasureList2, vTreasureList3, vTreasureList4, vTreasureList5, vTreasureList6, vTreasureList7,
                 vTreasureList8, vTreasureList9, vTreasureList10, vTreasureList11, vTreasureList12, vTreasureList13, vTreasureList14;

std::map<player_id, std::vector<int>> mTreasureHunterList;
std::vector<int> vTreasureHunterList1, vTreasureHunterList2, vTreasureHunterList3, vTreasureHunterList4, vTreasureHunterList5, vTreasureHunterList6, vTreasureHunterList7,
                 vTreasureHunterList8, vTreasureHunterList9, vTreasureHunterList10, vTreasureHunterList11, vTreasureHunterList12, vTreasureHunterList13, vTreasureHunterList14;

int cNumberPlayers = -1;

int cUnitTypeResource = -1;
int cUnitTypeHuntedResource = -1;
int cUnitTypeTree = -1;
int cUnitTypeAbstractVillager = -1;
int cUnitTypeAbstractWagon = -1;
int cUnitTypeHero = -1;
int cUnitTypeAbstractNugget = -1;
int cUnitTypeAbstractNuggetLand = -1;
int cUnitTypeConvertsHerds = -1;
int cUnitTypeGuardian = -1;
int cUnitTypeLogicalTypeLandMilitary = -1;

void aiStartSession(void)
{
    static bool init = true;

    if (!init)
        return;
    init = false;

    while(true)
    {
        cNumberPlayers++;
        if (!kbIsPlayerValid(cNumberPlayers))
            break;
    }

    array<wchar_t*>* unitTypes = GetUnitTypes();
    for(int i = 0 ; i < unitTypes->Count ; i++)
    {
        if (!wcscmp(unitTypes->Array[i], L"Resource"))
            cUnitTypeResource = i;
        if (!wcscmp(unitTypes->Array[i], L"HuntedResource"))
            cUnitTypeHuntedResource = i;
        if (!wcscmp(unitTypes->Array[i], L"Tree"))
            cUnitTypeTree = i;
        if (!wcscmp(unitTypes->Array[i], L"AbstractVillager"))
            cUnitTypeAbstractVillager = i;
        if (!wcscmp(unitTypes->Array[i], L"AbstractWagon"))
            cUnitTypeAbstractWagon = i;
        if (!wcscmp(unitTypes->Array[i], L"Hero"))
            cUnitTypeHero = i;
        if (!wcscmp(unitTypes->Array[i], L"AbstractNugget"))
            cUnitTypeAbstractNugget = i;
        if (!wcscmp(unitTypes->Array[i], L"AbstractNuggetLand"))
            cUnitTypeAbstractNuggetLand = i;
        if (!wcscmp(unitTypes->Array[i], L"ConvertsHerds"))
            cUnitTypeConvertsHerds = i;
        if (!wcscmp(unitTypes->Array[i], L"Guardian"))
            cUnitTypeGuardian = i;
        if (!wcscmp(unitTypes->Array[i], L"LogicalTypeLandMilitary"))
            cUnitTypeLogicalTypeLandMilitary = i;
    }

    mVillagerList[1] = vVillagerList1;
    mVillagerList[2] = vVillagerList2;
    mVillagerList[3] = vVillagerList3;
    mVillagerList[4] = vVillagerList4;
    mVillagerList[5] = vVillagerList5;
    mVillagerList[6] = vVillagerList6;
    mVillagerList[7] = vVillagerList7;
    mVillagerList[8] = vVillagerList8;
    mVillagerList[9] = vVillagerList9;
    mVillagerList[10] = vVillagerList10;
    mVillagerList[11] = vVillagerList11;
    mVillagerList[12] = vVillagerList12;
    mVillagerList[13] = vVillagerList13;
    mVillagerList[14] = vVillagerList14;

    mResourceList[1] = vResourceList1;
    mResourceList[2] = vResourceList2;
    mResourceList[3] = vResourceList3;
    mResourceList[4] = vResourceList4;
    mResourceList[5] = vResourceList5;
    mResourceList[6] = vResourceList6;
    mResourceList[7] = vResourceList7;
    mResourceList[8] = vResourceList8;
    mResourceList[9] = vResourceList9;
    mResourceList[10] = vResourceList10;
    mResourceList[11] = vResourceList11;
    mResourceList[12] = vResourceList12;
    mResourceList[13] = vResourceList13;
    mResourceList[14] = vResourceList14;

    mEasyList[1] = vEasyList1;
    mEasyList[2] = vEasyList2;
    mEasyList[3] = vEasyList3;
    mEasyList[4] = vEasyList4;
    mEasyList[5] = vEasyList5;
    mEasyList[6] = vEasyList6;
    mEasyList[7] = vEasyList7;
    mEasyList[8] = vEasyList8;
    mEasyList[9] = vEasyList9;
    mEasyList[10] = vEasyList10;
    mEasyList[11] = vEasyList11;
    mEasyList[12] = vEasyList12;
    mEasyList[13] = vEasyList13;
    mEasyList[14] = vEasyList14;

    mTreeList[1] = vTreeList1;
    mTreeList[2] = vTreeList2;
    mTreeList[3] = vTreeList3;
    mTreeList[4] = vTreeList4;
    mTreeList[5] = vTreeList5;
    mTreeList[6] = vTreeList6;
    mTreeList[7] = vTreeList7;
    mTreeList[8] = vTreeList8;
    mTreeList[9] = vTreeList9;
    mTreeList[10] = vTreeList10;
    mTreeList[11] = vTreeList11;
    mTreeList[12] = vTreeList12;
    mTreeList[13] = vTreeList13;
    mTreeList[14] = vTreeList14;

    mHuntList[1] = vHuntList1;
    mHuntList[2] = vHuntList2;
    mHuntList[3] = vHuntList3;
    mHuntList[4] = vHuntList4;
    mHuntList[5] = vHuntList5;
    mHuntList[6] = vHuntList6;
    mHuntList[7] = vHuntList7;
    mHuntList[8] = vHuntList8;
    mHuntList[9] = vHuntList9;
    mHuntList[10] = vHuntList10;
    mHuntList[11] = vHuntList11;
    mHuntList[12] = vHuntList12;
    mHuntList[13] = vHuntList13;
    mHuntList[14] = vHuntList14;

    mTreasureList[1] = vTreasureList1;
    mTreasureList[2] = vTreasureList2;
    mTreasureList[3] = vTreasureList3;
    mTreasureList[4] = vTreasureList4;
    mTreasureList[5] = vTreasureList5;
    mTreasureList[6] = vTreasureList6;
    mTreasureList[7] = vTreasureList7;
    mTreasureList[8] = vTreasureList8;
    mTreasureList[9] = vTreasureList9;
    mTreasureList[10] = vTreasureList10;
    mTreasureList[11] = vTreasureList11;
    mTreasureList[12] = vTreasureList12;
    mTreasureList[13] = vTreasureList13;
    mTreasureList[14] = vTreasureList14;

    mTreasureHunterList[1] = vTreasureHunterList1;
    mTreasureHunterList[2] = vTreasureHunterList2;
    mTreasureHunterList[3] = vTreasureHunterList3;
    mTreasureHunterList[4] = vTreasureHunterList4;
    mTreasureHunterList[5] = vTreasureHunterList5;
    mTreasureHunterList[6] = vTreasureHunterList6;
    mTreasureHunterList[7] = vTreasureHunterList7;
    mTreasureHunterList[8] = vTreasureHunterList8;
    mTreasureHunterList[9] = vTreasureHunterList9;
    mTreasureHunterList[10] = vTreasureHunterList10;
    mTreasureHunterList[11] = vTreasureHunterList11;
    mTreasureHunterList[12] = vTreasureHunterList12;
    mTreasureHunterList[13] = vTreasureHunterList13;
    mTreasureHunterList[14] = vTreasureHunterList14;
}

int kbUnitCountByLoc(int owner = -1, int unit_type = -1, int unit_state = -1, syscalls::vector position = cOriginVector)
{
    int q = kbUnitQueryCreate("q");
    return -1;
}

void aiVillagerListUpdate(void)
{
    aiStartSession();

    const int ctxPlayer = xsGetContextPlayer();

    mVillagerList.at(ctxPlayer).clear();

    int q = kbUnitQueryCreate("AliceTemporaryQuery");
    kbUnitQuerySetIgnoreKnockedOutUnits(q, true);
    kbUnitQuerySetState(q, cUnitStateAlive);
    kbUnitQuerySetPlayerRelation(q, -1);
    kbUnitQuerySetPlayerID(q, ctxPlayer, false);
    kbUnitQuerySetUnitType(q, cUnitTypeAbstractVillager);
    int count = kbUnitQueryExecute(q);

    for(int i = 0 ; i < count ; i++)
        mVillagerList.at(ctxPlayer).push_back(kbUnitQueryGetResult(q, i));

    kbUnitQueryDestroy(q);
}

int aiGetClosestVillagerIndex(syscalls::vector center = cOriginVector)
{
    aiStartSession();

    const int ctxPlayer = xsGetContextPlayer();

    float smallest_distance = 999999;
    int closest_unit_index = -1;

    for(size_t i = 0 ; i < mVillagerList.at(ctxPlayer).size() ; i++)
    {
        int villager = mVillagerList.at(ctxPlayer).at(i);
        trUnitSelectByID(villager);
        if (trUnitDistanceToPoint(center->x, center->y, center->z) < smallest_distance)
        {
            smallest_distance = trUnitDistanceToPoint(center->x, center->y, center->z);
            closest_unit_index = i;
        }
    }

    return closest_unit_index;
}

int aiGetVillagerFromList(int index = -1)
{
    aiStartSession();

    const int ctxPlayer = xsGetContextPlayer();

    if (mVillagerList.at(ctxPlayer).size() == 0)
        return -1;
    if (index <= -1)
        return mVillagerList.at(ctxPlayer).at(aiRandInt(mVillagerList.at(ctxPlayer).size()));
    if ((size_t)index >= mVillagerList.at(ctxPlayer).size())
        return -1;
    return mVillagerList.at(ctxPlayer).at(index);
}

void aiVillagerListPopElementAt(int index = -1)
{
    aiStartSession();

    const int ctxPlayer = xsGetContextPlayer();

    if (mVillagerList.at(ctxPlayer).size() <= 0)
        return;
    if (index <= -1)
        return;
    if ((size_t)index >= mVillagerList.at(ctxPlayer).size())
        return;
    mVillagerList.at(ctxPlayer).erase(mVillagerList.at(ctxPlayer).cbegin() + index);
}

void aiResourceListUpdate(void)
{
    aiStartSession();

    const int ctxPlayer = xsGetContextPlayer();

    mResourceList.at(ctxPlayer).clear();
    mEasyList.at(ctxPlayer).clear();
    mTreeList.at(ctxPlayer).clear();
    mHuntList.at(ctxPlayer).clear();

    _vector *center = new _vector(0, 0, 0);
    center = kbBaseGetLocation(center, ctxPlayer, kbBaseGetMainID(ctxPlayer));
    xsSetContextPlayer(0);
    kbLookAtAllUnitsOnMap();
    int q = kbUnitQueryCreate("AliceTemporaryQuery");
    kbUnitQuerySetIgnoreKnockedOutUnits(q, true);
    kbUnitQuerySetState(q, cUnitStateAny);
    kbUnitQuerySetAscendingSort(q, true);
    kbUnitQuerySetPosition(q, center);
    kbUnitQuerySetMaximumDistance(q, 5000);
    kbUnitQuerySetPlayerID(q, -1, false);
    kbUnitQuerySetPlayerRelation(q, cPlayerRelationAny);
    kbUnitQuerySetUnitType(q, cUnitTypeResource);
    int count = kbUnitQueryExecute(q);
    for(int i = 0; i < count ; i++)
    {
        int resource = kbUnitQueryGetResult(q, i);
        if (kbUnitGetPlayerID(resource) == -1)
            continue;
        mResourceList.at(ctxPlayer).push_back(resource);
        if (kbUnitIsType(resource, cUnitTypeHuntedResource) == false && kbUnitIsType(resource, cUnitTypeTree) == false)
            mEasyList.at(ctxPlayer).push_back(resource);
        if (kbUnitIsType(resource, cUnitTypeTree) == true)
            mTreeList.at(ctxPlayer).push_back(resource);
        if (kbUnitIsType(resource, cUnitTypeHuntedResource) == true && kbUnitIsType(resource, cUnitTypeTree) == false)
            mHuntList.at(ctxPlayer).push_back(resource);
    }
    kbUnitQueryDestroy(q);
    xsSetContextPlayer(ctxPlayer);
    delete center;
}

int aiGetEasyResource(int index = -1)
{
    aiStartSession();

    const int ctxPlayer = xsGetContextPlayer();

    if (mEasyList.at(ctxPlayer).size() == 0)
        return -1;
    if (index <= -1)
        return mEasyList.at(ctxPlayer).at(aiRandInt(mEasyList.at(ctxPlayer).size()));
    if ((size_t)index >= mEasyList.at(ctxPlayer).size())
        return -1;
    return mEasyList.at(ctxPlayer).at(index);
}

int aiGetTreeResource(int index = -1)
{
    aiStartSession();

    const int ctxPlayer = xsGetContextPlayer();

    if (mTreeList.at(ctxPlayer).size() == 0)
        return -1;
    if (index <= -1)
        return mTreeList.at(ctxPlayer).at(aiRandInt(mTreeList.at(ctxPlayer).size()));
    if ((size_t)index >= mTreeList.at(ctxPlayer).size())
        return -1;
    return mTreeList.at(ctxPlayer).at(index);
}

int aiGetHuntResource(int index = -1)
{
    aiStartSession();

    const int ctxPlayer = xsGetContextPlayer();

    if (mHuntList.at(ctxPlayer).size() == 0)
        return -1;
    if (index <= -1)
        return mHuntList.at(ctxPlayer).at(aiRandInt(mHuntList.at(ctxPlayer).size()));
    if ((size_t)index >= mHuntList.at(ctxPlayer).size())
        return -1;
    return mHuntList.at(ctxPlayer).at(index);
}

void aiActivateTech(int techID = -1)
{
    trTechSetStatus(xsGetContextPlayer(), techID, cTechStatusActive);
}

void aiTreasureListUpdate(void)
{
    aiStartSession();
    const int ctxPlayer = xsGetContextPlayer();
    int q = kbUnitQueryCreate("AliceTemporaryQuery");
    kbUnitQuerySetIgnoreKnockedOutUnits(q, false);
    kbUnitQuerySetPlayerID(q, 0);
    kbUnitQuerySetState(q, cUnitStateAny);
    kbUnitQuerySetUnitType(q, cUnitTypeAbstractNugget);
    for(int i = 0 ; i < kbUnitQueryExecute(q) ; i++)
        mTreasureList.at(ctxPlayer).push_back(kbUnitQueryGetResult(q, i));
    std::sort(mTreasureList.at(ctxPlayer).begin(), mTreasureList.at(ctxPlayer).end());
    kbUnitQueryDestroy(q);
}

int aiGetTreasureIndex(int treasureID = -1)
{
    const int ctxPlayer = xsGetContextPlayer();
    for(size_t i = 0 ; i < mTreasureList.at(ctxPlayer).size() ; i++)
    {
        if (mTreasureList.at(ctxPlayer).at(i) == treasureID)
            return i;
    }
    return -1;
}

int aiGetClosestTreasure(syscalls::vector position = cOriginVector)
{
    int temp1 = 999999, temp = -1;
    int q = kbUnitQueryCreate("AliceTemporaryQuery");
    kbUnitQuerySetUnitType(q, cUnitTypeAbstractNuggetLand);
    kbUnitQuerySetPlayerRelation(q, -1);
    kbUnitQuerySetPlayerID(q, 0, false);
    kbUnitQuerySetState(q, cUnitStateAlive);
    kbUnitQuerySetIgnoreKnockedOutUnits(q, true);
    kbUnitQuerySetAscendingSort(q, true);
    kbUnitQuerySetPosition(q, position);
    kbUnitQuerySetMaximumDistance(q, 400.0);
    for(int i = 0 ; i < kbUnitQueryExecute(q) ; i++)
    {
        syscalls::_vector *nugget_loc = new _vector(0, 0, 0);
        if (kbAreaGroupGetIDByPosition(position) != kbAreaGroupGetIDByPosition(kbUnitGetPosition(nugget_loc, kbUnitQueryGetResult(q, i))))
        {
            delete nugget_loc;
            continue;
        }
        trUnitSelectClear();
        trUnitSelectByID(kbUnitQueryGetResult(q, i));
        float distance = trUnitDistanceToPoint(position->x, position->y, position->z);
        trUnitSelectClear();
        if (distance >= 60.0)
        {
            int nugget_area = kbAreaGetIDByPosition(kbUnitGetPosition(nugget_loc, kbUnitQueryGetResult(q, i)));
            if ((float)((float)kbAreaGetNumberFogTiles(nugget_area) +
                        (float)kbAreaGetNumberVisibleTiles(nugget_area))/(float)kbAreaGetNumberTiles(nugget_area) < 0.65)
            {
                delete nugget_loc;
                continue;
            }
        }
        pstring convertible = kbGetProtoUnitName(kbUnitGetProtoUnitID(kbUnitQueryGetResult(q, i)));
        if (std::strcmp(convertible, "ypNuggetTreeAsian") == 0 ||
                std::strcmp(convertible, "NuggetWolfTreebent") == 0 ||
                std::strcmp(convertible, "NuggetWolfRock") == 0 ||
                std::strcmp(convertible, "NuggetBearTree") == 0 ||
                std::strcmp(convertible, "NuggetKidnap") == 0 ||
                std::strcmp(convertible, "NuggetKidnapBrit") == 0 ||
                std::strcmp(convertible, "ypNuggetKidnapAsian") == 0)
        {
            syscalls::_vector *convertible_loc = new _vector(0, 0, 0);
            int s = kbUnitQueryCreate("AliceTemporarySearch");
            kbUnitQuerySetUnitType(s, cUnitTypeConvertsHerds);
            kbUnitQuerySetPlayerRelation(s, -1);
            kbUnitQuerySetPlayerID(s, 0, false);
            kbUnitQuerySetState(s, cUnitStateAlive);
            kbUnitQuerySetIgnoreKnockedOutUnits(s, true);
            kbUnitQuerySetAscendingSort(s, true);
            kbUnitQuerySetPosition(s, kbUnitGetPosition(convertible_loc, kbUnitQueryGetResult(q, i)));
            kbUnitQuerySetMaximumDistance(s, 30.0);
            int count = kbUnitQueryExecute(s);
            kbUnitQueryDestroy(s);
            delete convertible_loc;
            if (count == 0)
                continue;
        }
        syscalls::_vector *treasure_loc = new _vector(0, 0, 0);
        int s = kbUnitQueryCreate("AliceTemporarySearch");
        kbUnitQuerySetIgnoreKnockedOutUnits(s, true);
        kbUnitQuerySetState(s, cUnitStateAlive);
        kbUnitQuerySetPlayerRelation(s, -1);
        kbUnitQuerySetPlayerID(s, 0, false);
        kbUnitQuerySetPosition(s, kbUnitGetPosition(treasure_loc, kbUnitQueryGetResult(q, i)));
        kbUnitQuerySetMaximumDistance(s, 30.0);
        kbUnitQuerySetUnitType(s, cUnitTypeGuardian);
        int count = kbUnitQueryExecute(s);
        kbUnitQueryDestroy(s);
        if (count < 10)
        {
            s = kbUnitQueryCreate("AliceTemporarySearch");
            kbUnitQuerySetPlayerRelation(s, -1);
            kbUnitQuerySetPlayerID(s, 0, false);
            kbUnitQuerySetUnitType(s, cUnitTypeGuardian);
            kbUnitQuerySetPosition(s, kbUnitGetPosition(treasure_loc, kbUnitQueryGetResult(q, i)));
            kbUnitQuerySetMaximumDistance(s, 30.0);
            kbUnitQueryExecute(s);
            if (kbUnitQueryGetUnitHitpoints(s, true) < temp1)
            {
                temp = kbUnitQueryGetResult(q, i);
                temp1 = kbUnitQueryGetUnitHitpoints(s, true);
            }
            kbUnitQueryDestroy(s);
        }
        delete treasure_loc;
    }
    kbUnitQueryDestroy(q);

    return temp;
}

float aiGetTreasureDifficulty(int treasureID = -1)
{
    syscalls::_vector *treasure_loc = new syscalls::_vector(9, 0, 0);
    int s = kbUnitQueryCreate("AliceTemporarySearch");
    kbUnitQuerySetPlayerRelation(s, -1);
    kbUnitQuerySetPlayerID(s, 0, false);
    kbUnitQuerySetUnitType(s, cUnitTypeGuardian);
    kbUnitQuerySetPosition(s, kbUnitGetPosition(treasure_loc, treasureID));
    kbUnitQuerySetMaximumDistance(s, 30.0);
    kbUnitQueryExecute(s);
    float difficulty = kbUnitQueryGetUnitHitpoints(s, true);
    kbUnitQueryDestroy(s);
    return difficulty;
}

void aiTreasureHunterListUpdate(void)
{
    aiStartSession();

    const int ctxPlayer = xsGetContextPlayer();

    mTreasureHunterList.at(ctxPlayer).clear();

    int q = kbUnitQueryCreate("AliceTemporaryQuery");
    kbUnitQuerySetIgnoreKnockedOutUnits(q, true);
    kbUnitQuerySetState(q, cUnitStateAlive);
    kbUnitQuerySetPlayerRelation(q, -1);
    kbUnitQuerySetPlayerID(q, ctxPlayer, false);
    kbUnitQuerySetUnitType(q, cUnitTypeLogicalTypeLandMilitary);
    int count = kbUnitQueryExecute(q);

    for(int i = 0 ; i < count ; i++)
    {
        if (mTreasureHunterList.at(ctxPlayer).size() >= 15)
            break;
        int hunter = kbUnitQueryGetResult(q, i);
        if (kbUnitGetPlanID(hunter) >= 0)
            continue;
        mTreasureHunterList.at(ctxPlayer).push_back(hunter);
    }

    kbUnitQueryDestroy(q);
}

int aiGetTreasureHunterFromList(int index = -1)
{
    aiStartSession();

    const int ctxPlayer = xsGetContextPlayer();

    if (mTreasureHunterList.at(ctxPlayer).size() == 0)
        return -1;
    if (index <= -1)
        return mTreasureHunterList.at(ctxPlayer).at(aiRandInt(mTreasureHunterList.at(ctxPlayer).size()));
    if ((size_t)index >= mTreasureHunterList.at(ctxPlayer).size())
        return -1;
    return mTreasureHunterList.at(ctxPlayer).at(index);
}

void aiTreasureHunterListPopElementAt(int index = -1)
{
    aiStartSession();

    const int ctxPlayer = xsGetContextPlayer();

    if (mTreasureHunterList.at(ctxPlayer).size() <= 0)
        return;
    if (index <= -1)
        return;
    if ((size_t)index >= mTreasureHunterList.at(ctxPlayer).size())
        return;
    mTreasureHunterList.at(ctxPlayer).erase(mTreasureHunterList.at(ctxPlayer).cbegin() + index);
}

extern "C" __declspec(dllexport) int UHCPluginMain(UHCPluginInfo *pluginInfo)
{
    bool bfalse = false;
    bool btrue = true;
    int intneg = -1;
    int intnul = 0;
    float floatneg = -1.0;
    float floatnul = 0.0;
    float floatful = 1.0;
    float pi = 3.141592;
    syscalls::string strnul = "";

    pluginInfo->RegisterSyscall(GroupAI, SyscallVoid, "aiStartSession", aiStartSession, 0, "void aiStartSession(void)");
    pluginInfo->RegisterSyscall(GroupAI, SyscallVoid, "aiVillagerListUpdate", aiVillagerListUpdate, 0, "void aiVillagerListUpdate(void)");
    UHCSyscall& sGetClosestVillagrIndex = pluginInfo->RegisterSyscall(GroupAI, SyscallInteger, "aiGetClosestVillagerIndex", aiGetClosestVillagerIndex, 1, "int aiGetClosestVillagerIndex(vector center)");
    pluginInfo->SyscallSetParam(sGetClosestVillagrIndex, 0, SyscallVector, cOriginVector);
    UHCSyscall& sGetVillagerFromList = pluginInfo->RegisterSyscall(GroupAI, SyscallInteger, "aiGetVillagerFromList", aiGetVillagerFromList, 1, "int aiGetVillagerFromList(int index)");
    pluginInfo->SyscallSetParam(sGetVillagerFromList, 0, SyscallInteger, &intneg);
    UHCSyscall& sVillagerListPopElementAt = pluginInfo->RegisterSyscall(GroupAI, SyscallVoid, "aiVillagerListPopElementAt", aiVillagerListPopElementAt, 1, "void aiVillagerListPopElementAt(int index)");
    pluginInfo->SyscallSetParam(sVillagerListPopElementAt, 0, SyscallInteger, &intneg);
    pluginInfo->RegisterSyscall(GroupAI, SyscallVoid, "aiResourceListUpdate", aiResourceListUpdate, 0, "void aiResourceListUpdate(void)");
    UHCSyscall& sGetEasyResource = pluginInfo->RegisterSyscall(GroupAI, SyscallInteger, "aiGetEasyResource", aiGetEasyResource, 2, "int aiGetEasyResource(int index, bool refresh)");
    pluginInfo->SyscallSetParam(sGetEasyResource, 0, SyscallInteger, &intneg);
    pluginInfo->SyscallSetParam(sGetEasyResource, 1, SyscallBool, &bfalse);
    UHCSyscall& sGetTreeResource = pluginInfo->RegisterSyscall(GroupAI, SyscallInteger, "aiGetTreeResource", aiGetTreeResource, 2, "int aiGetTreeResource(int index, bool refresh)");
    pluginInfo->SyscallSetParam(sGetTreeResource, 0, SyscallInteger, &intneg);
    pluginInfo->SyscallSetParam(sGetTreeResource, 1, SyscallBool, &bfalse);
    UHCSyscall& sGetHuntResource = pluginInfo->RegisterSyscall(GroupAI, SyscallInteger, "aiGetHuntResource", aiGetHuntResource, 2, "int aiGetHuntResource(int index, bool refresh)");
    pluginInfo->SyscallSetParam(sGetHuntResource, 0, SyscallInteger, &intneg);
    pluginInfo->SyscallSetParam(sGetHuntResource, 1, SyscallBool, &bfalse);
    pluginInfo->RegisterSyscall(GroupAI, SyscallVoid, "aiTreasureHunterListUpdate", aiTreasureHunterListUpdate, 0, "void aiTreasureHunterListUpdate(void)");
    UHCSyscall& sGetTreasureHunterFromList = pluginInfo->RegisterSyscall(GroupAI, SyscallInteger, "aiGetTreasureHunterFromList", aiGetTreasureHunterFromList, 1, "int aiGetTreasureHunterFromList(int index)");
    pluginInfo->SyscallSetParam(sGetTreasureHunterFromList, 0, SyscallInteger, &intneg);
    UHCSyscall& sTreasureHunterListPopElementAt = pluginInfo->RegisterSyscall(GroupAI, SyscallVoid, "aiTreasureHunterListPopElementAt", aiTreasureHunterListPopElementAt, 1, "void aiTreasureHunterListPopElementAt(int index)");
    pluginInfo->SyscallSetParam(sTreasureHunterListPopElementAt, 0, SyscallInteger, &intneg);
    pluginInfo->RegisterSyscall(GroupAI, SyscallVoid, "aiTreasureListUpdate", aiTreasureListUpdate, 0, "void aiTreasureListUpdate(void)");
    UHCSyscall& sGetTreasureIndex = pluginInfo->RegisterSyscall(GroupAI, SyscallInteger, "aiGetTreasureIndex", aiGetTreasureIndex, 1, "int aiGetTreasureIndex(int treasureID)");
    pluginInfo->SyscallSetParam(sGetTreasureIndex, 0, SyscallInteger, &intneg);
    UHCSyscall& sGetClosestTreasure = pluginInfo->RegisterSyscall(GroupAI, SyscallInteger, "aiGetClosestTreasure", aiGetClosestTreasure, 1, "int aiGetClosestTreasure(vector position)");
    pluginInfo->SyscallSetParam(sGetClosestTreasure, 0, SyscallVector, cOriginVector);
    UHCSyscall& sGetTreasureDifficulty = pluginInfo->RegisterSyscall(GroupAI, SyscallFloat, "aiGetTreasureDifficulty", aiGetTreasureDifficulty, 1, "float aiGetTreasureDifficulty(int treasureID)");
    pluginInfo->SyscallSetParam(sGetTreasureDifficulty, 0, SyscallInteger, &intneg);
    UHCSyscall& sActivateTech = pluginInfo->RegisterSyscall(GroupAI, SyscallVoid, "aiActivateTech", aiActivateTech, 1, "void aiActivateTech(int techID)");
    pluginInfo->SyscallSetParam(sActivateTech, 0, SyscallInteger, &intneg);
    UHCSyscall& sSin = pluginInfo->RegisterSyscall(GroupXS, SyscallFloat, "xsSin", sinus, 1, "float xsSin(float x) -- returns sin(x)");
    pluginInfo->SyscallSetParam(sSin, 0, SyscallFloat, &pi);
    UHCSyscall& sCos = pluginInfo->RegisterSyscall(GroupXS, SyscallFloat, "xsCos", cosinus, 1, "float xsCos(float x) -- returns cos(x)");
    pluginInfo->SyscallSetParam(sCos, 0, SyscallFloat, &pi);
    UHCSyscall& sTan = pluginInfo->RegisterSyscall(GroupXS, SyscallFloat, "xsTan", tangent, 1, "float xsTan(float x) -- returns tan(x)");
    pluginInfo->SyscallSetParam(sTan, 0, SyscallFloat, &pi);
    UHCSyscall& sSqrt = pluginInfo->RegisterSyscall(GroupXS, SyscallFloat, "xsSqrt", square_root, 1, "float xsSqrt(float x) -- returns sqrt(x)");
    pluginInfo->SyscallSetParam(sSqrt, 0, SyscallFloat, &pi);
    UHCSyscall& sASin = pluginInfo->RegisterSyscall(GroupXS, SyscallFloat, "xsASin", arcsinus, 1, "float xsASin(float x) -- returns asin(x)");
    pluginInfo->SyscallSetParam(sASin, 0, SyscallFloat, &floatnul);
    UHCSyscall& sACos = pluginInfo->RegisterSyscall(GroupXS, SyscallFloat, "xsACos", arccosinus, 1, "float xsACos(float x) -- returns acos(x)");
    pluginInfo->SyscallSetParam(sACos, 0, SyscallFloat, &floatnul);
    UHCSyscall& sATan = pluginInfo->RegisterSyscall(GroupXS, SyscallFloat, "xsATan", arctangent, 1, "float xsATan(float x) -- returns atan(x)");
    pluginInfo->SyscallSetParam(sATan, 0, SyscallFloat, &floatnul);
    UHCSyscall& sATan2 = pluginInfo->RegisterSyscall(GroupXS, SyscallFloat, "xsATan2", arctangent2, 2, "float xsATan2(float a, float b) -- returns atan2(a, b)");
    pluginInfo->SyscallSetParam(sATan2, 0, SyscallFloat, &floatnul);
    pluginInfo->SyscallSetParam(sATan2, 1, SyscallFloat, &floatnul);
    UHCSyscall& sAbs = pluginInfo->RegisterSyscall(GroupXS, SyscallFloat, "xsAbs", absolute_value, 1, "float xsAbs(float x)");
    pluginInfo->SyscallSetParam(sAbs, 0, SyscallFloat, &floatnul);
    UHCSyscall& sCeil = pluginInfo->RegisterSyscall(GroupXS, SyscallFloat, "xsCeil", ceiling, 1, "float xsCeil(float x)");
    pluginInfo->SyscallSetParam(sCeil, 0, SyscallFloat, &floatnul);
    UHCSyscall& sFloor = pluginInfo->RegisterSyscall(GroupXS, SyscallFloat, "xsFloor", flooring, 1, "float xsFloor(float x)");
    pluginInfo->SyscallSetParam(sFloor, 0, SyscallFloat, &floatnul);
    UHCSyscall& sMin = pluginInfo->RegisterSyscall(GroupXS, SyscallFloat, "xsMin", minimum, 2, "float xsMin(float a, float b)");
    pluginInfo->SyscallSetParam(sMin, 0, SyscallFloat, &floatnul);
    pluginInfo->SyscallSetParam(sMin, 1, SyscallFloat, &floatnul);
    UHCSyscall& sMax = pluginInfo->RegisterSyscall(GroupXS, SyscallFloat, "xsMax", maximum, 2, "float xsMax(float a, float b)");
    pluginInfo->SyscallSetParam(sMax, 0, SyscallFloat, &floatnul);
    pluginInfo->SyscallSetParam(sMax, 1, SyscallFloat, &floatnul);
    return 0;
}







































